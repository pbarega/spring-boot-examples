package org.ftp.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FtpBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(FtpBatchApplication.class, args);
	}
}
