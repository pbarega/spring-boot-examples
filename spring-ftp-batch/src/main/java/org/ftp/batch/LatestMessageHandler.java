package org.ftp.batch;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@EnableBatchProcessing
public class LatestMessageHandler implements MessageHandler {

	private String[] content;

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	RestTemplate template = new RestTemplate();

	private String url="http://localhost:8080/customer/created";

	private String path;
	@Override
	public void handleMessage(Message<?> source) throws MessagingException {
		try {
/*			String fileContent = new String(Files.readAllBytes(Paths.get(source.getPayload().toString())));
			System.out.println(fileContent);
			this.content = fileContent.split(BatchConstants.SAPERATER);
			JobParameters jobParameter=new JobParameters();
			jobLauncher.run(importUserJob(), jobParameter);*/
			path=source.getPayload().toString();
			JobParameters jobParameter=new JobParameters();
			jobLauncher.run(importUserJob(path), jobParameter);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	/**
	 * 
	 * @return gets the content of the file.
	 */
	public String[] getContent() {
		return content;
	}
	@Scope(scopeName="prototype")
	public Job importUserJob(String path) {
		return jobBuilderFactory.get("importUserJob").incrementer(new RunIdIncrementer()).flow(step1(path)).end().build();
	}
	
	public Step step1(String path) {
		return stepBuilderFactory.get("step1").allowStartIfComplete(true).<Customer, Customer>chunk(10).reader(reader(path)).writer(writer()).build();
	}

	private ItemWriter<? super Customer> writer() {
		/*return (customers) -> {
			System.out.println(customers.size());
			for(Customer customer:customers){
				System.out.println(template.postForObject(url,customer, String.class));
			}
		};*/
		
		ItemWriter <Customer> writer= new ItemWriter<Customer>() {
			@Override
			public void write(List<? extends Customer> items) throws Exception {
			System.out.println(items.size());
			}
		};
		return writer;
		
	}
	
	public LineMapper<Customer> lineMapper() {
		DefaultLineMapper<Customer> lineMapper = new DefaultLineMapper<Customer>();

		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setDelimiter(";");
		lineTokenizer.setStrict(false);
		lineTokenizer.setNames(new String[]{"firstName","lastName","age","gender","address1","city","country","pin"});
		BeanWrapperFieldSetMapper<Customer> fieldSetMapper = new BeanWrapperFieldSetMapper<Customer>();
		fieldSetMapper.setTargetType(Customer.class);

		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);

		return lineMapper;
	}
	

	private ItemReader<? extends Customer> reader(String path) {
		FlatFileItemReader<Customer> itemReader= new FlatFileItemReader<>();
		itemReader.setResource(new FileSystemResource(path));
		itemReader.setLineMapper(lineMapper());
		return itemReader;
		/*return () -> {
			Customer customer= new Customer();
			customer.setFirstName(this.content[0]);
			customer.setLastName(this.content[1]);
			customer.setAge(this.content[2]);
			customer.setGender(this.content[3]);
			customer.setAddress1(this.content[4]);
			customer.setCity(this.content[5]);
			customer.setCountry(this.content[6]);
			customer.setPin(this.content[7]);
			return customer;
		};*/
	}
}
